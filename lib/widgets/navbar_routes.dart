import 'package:flutter/material.dart';
import 'package:helping_hands/pages/opportunities.dart';
import 'package:helping_hands/pages/history.dart';
import 'package:helping_hands/pages/home.dart';
import 'package:helping_hands/pages/profile.dart';

class NavBar extends StatefulWidget {


  @override
  State<NavBar> createState() => _NavBarState();
}


class _NavBarState extends State<NavBar> {
  int _selectedIndex = 0;
  final _screens = [
    Home(),
    Opportunities(),
    Attendance(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFFbbe7d7);
    return Scaffold(
      backgroundColor: Colors.white,
      body: _screens[_selectedIndex],
      bottomNavigationBar: Container(
        height: 80,
        child: BottomNavigationBar(
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: customGreenColor,
          selectedLabelStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          currentIndex: _selectedIndex,
          onTap: (index){
            setState(() {
              _selectedIndex = index;
            });
          }, items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_filled),
              label: "Home"
            ),
          BottomNavigationBarItem(
              icon: Icon(Icons.group_add),
              label: "Opportunities"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.check_box_outlined),
              label: "Attendance"
          ),
        ],


        ),
      ),


    );
  }
}
