import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:helping_hands/constant/utils.dart';

Future userLogin(String email, String password) async {
  final Uri uri = Uri.parse('${Utils.baseUrl}/users/login'); // Create a Uri object

  final response = await http.post(
    uri, // Pass the Uri object here
    headers: {"Accept": "application/json"},
    body: {'email': email, 'password': password},
  );

  if (response.statusCode == 200) {
    var decodedData = jsonDecode(response.body);
    return decodedData;
  } else {
    print('HTTP Error: ${response.statusCode}');
    return null;
  }
}

Future userRegister(String firstName,String lastName,String email,String mobileNumber, String password) async {
  final Uri uri = Uri.parse('${Utils.baseUrl}/users/register'); // Create a Uri object

  final response = await http.post(
    uri, // Pass the Uri object here
    headers: {"Accept": "application/json"},
    body: {'firstName': firstName,'lastName': lastName,'mobileNumber': mobileNumber,'email': email, 'password': password},
  );

  if (response.statusCode == 200) {
    var decodedData = jsonDecode(response.body);
    return decodedData;
  } else {
    print('HTTP Error: ${response.statusCode}');
    return null;
  }

}





