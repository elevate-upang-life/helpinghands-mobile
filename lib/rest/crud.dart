import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import '../models/event.dart';

class Crud {
  static const baseUrl = "http://192.168.137.1:3000";

  //add method
  static Future<void> addEvent(Map<String, dynamic> edata, File imageFile) async {
    print(edata);

    // Create a multipart request
    var request = http.MultipartRequest('POST', Uri.parse('$baseUrl/api/add_event'));

    // Add event data fields
    request.fields['eevent'] = edata['eevent'];
    request.fields['etime'] = edata['etime'];
    request.fields['edescription'] = edata['edescription'];
    request.fields['elocation'] = edata['elocation'];


    // Extract the original filename and extension
    final originalFileName = imageFile.path.split('/').last;
    final originalFileExtension = originalFileName.split('.').last;

    // Use the extracted filename and extension when setting the filename in your request
    final filename = '$originalFileName.$originalFileExtension';

    // Add the image file to the request using the extracted filename
    request.files.add(http.MultipartFile(
      'image',
      imageFile.readAsBytes().asStream(),
      imageFile.lengthSync(),
      filename: filename, // Use the extracted filename
    ));

    try {
      final streamedResponse = await request.send();
      final res = await http.Response.fromStream(streamedResponse);

      if (res.statusCode == 200 || res.statusCode == 201) {
        var data = jsonDecode(res.body.toString());
        print(data);
      } else {
        throw Exception('Failed to add event: ${res.statusCode}');
      }
    } catch (e) {
      debugPrint(e.toString());
      throw Exception('Failed to add event: $e');
    }
  }




  //get method
  static Future<List<Event>> getEvent() async {
    List<Event> events = [];

    var url = Uri.parse("$baseUrl/api/get_event");

    try {
      final res = await http.get(url);

      if (res.statusCode == 200) {
        var data = jsonDecode(res.body);
        print(data); // Print the response data for debugging

        for (var value in data['events']) {
          events.add(
            Event(
              event: value['eevent'],
              time: value['etime'],
              description: value['edescription'],
              location: value['elocation'],
              id: value['id'].toString(),
              image: value['image'], // Ensure 'image' is included in the event data
            ),
          );
        }

        return events;
      } else {
        throw Exception('Failed to get events: ${res.statusCode}');
      }
    } catch (e) {
      debugPrint(e.toString());
      throw Exception('Failed to get events: $e');
    }
  }


  //update put method
  static Future<void> updateEvent(String eventId, Map<String, dynamic> data, String? imagePath) async {
    // Create a multipart request
    var request = http.MultipartRequest('PUT', Uri.parse('$baseUrl/api/update_event/$eventId'));

    // Add event data fields
    request.fields['eevent'] = data['eevent'];
    request.fields['etime'] = data['etime'];
    request.fields['elocation'] = data['elocation'];
    request.fields['edescription'] = data['edescription'];

    // If a new image is selected, add it to the request
    if (imagePath != null) {
      final file = File(imagePath);
      final fileName = path.basename(imagePath); // Extract the file name from the path
      request.files.add(http.MultipartFile(
        'image',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: fileName, // Use the extracted file name
      ));
    }

    try {
      print('Sending update request...');
      final streamedResponse = await request.send();
      print('Request sent, awaiting response...');

      final res = await http.Response.fromStream(streamedResponse);
      print('Response received.');

      print('Response status code: ${res.statusCode}');
      print('Response body: ${res.body}');

      if (res.statusCode == 200) {
        try {
          var responseData = jsonDecode(res.body);
          print(responseData);
        } catch (e) {
          debugPrint('Failed to decode response data: $e');
          throw Exception('Failed to update event: Failed to decode response data');
        }
      } else {
        debugPrint('Failed to update event: ${res.statusCode}');
        throw Exception('Failed to update event: ${res.statusCode}');
      }
    } catch (e) {
      debugPrint('Failed to send or receive the update request: $e');
      throw Exception('Failed to update event: $e');
    }
  }




  //delete method

  static Future<void> deleteEvent(id) async {
    var url = Uri.parse("$baseUrl/api/delete/$id");

    try {
      final res = await http.delete(url);

      if (res.statusCode == 204) {
        print('Event deleted successfully');
      } else {
        print('Failed to delete. Status code: ${res.statusCode}');
      }
    } catch (e) {
      debugPrint('Failed to delete event: $e');
    }
  }

}
