import 'package:easy_splash_screen/easy_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:helping_hands/pages/login.dart';
import 'pages/register.dart';
import 'pages/home.dart';

void main() => runApp(MaterialApp(
      home: EasySplashScreen(
        logo: Image.asset('lib/images/splash.png'),
        durationInSeconds: 3,
        backgroundColor: Color(0xFFbbe7d7),
        title: Text(
          'HELPING HANDS',
          style: TextStyle(
              fontSize: 25,
              fontFamily: 'Cupertino',
              fontWeight: FontWeight.bold,
              color: Colors.black54),
        ),
        loadingText: const Text(
          'Elevate Life',
          style: TextStyle(color: Colors.black54),
        ),

        logoWidth: 200,
        navigator: Login(),

      ),
      routes: {
        '/login': (context) => Login(),
        '/register': (context) => Register(),
        '/home': (context) => Home()
      },
      debugShowCheckedModeBanner: false,
    ));
