import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:helping_hands/pages/login.dart';
import 'package:helping_hands/rest/rest-api.dart';

class Register extends StatefulWidget {
  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool hidePassword = true;
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFFbbe7d7);
    return SafeArea(
      child: Scaffold(
        backgroundColor: customGreenColor,
        body: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('lib/images/register.png'),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: TextFormField(
                            controller: firstNameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              hintText: "First Name",
                              hintStyle: TextStyle(color: Colors.black54),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "First Name can't be empty.";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                          width: 16.0), // Add some spacing between the fields
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: TextFormField(
                            controller: lastNameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Colors.black54),
                              ),
                              hintText: "Last Name",
                              hintStyle: TextStyle(color: Colors.black54),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Last Name can't be empty.";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ), // Set
                    child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        hintText: "Email Address",
                        hintStyle: TextStyle(color: Colors.black54),
                        // Add your customizations here
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Email can't be empty.";
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ), // Set
                    child: TextFormField(
                      controller: mobileNumberController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        hintText: "Mobile Number",
                        hintStyle: TextStyle(color: Colors.black54),
                        // Add your customizations here
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Mobile Number can't be empty.";
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ), // Set
                    child: TextFormField(
                      controller: passwordController,
                      obscureText: hidePassword,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: Colors.black54),
                        ),
                        hintText: "Password",
                        hintStyle: TextStyle(color: Colors.black54),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          color: Colors.black54,
                          icon: Icon(
                            hidePassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                          ),
                        ),
                        // Add your customizations here
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Password can't be empty.";
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 25),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        // Access the text from the controllers here
                        final firstName = firstNameController.text;
                        final lastName = lastNameController.text;
                        final email = emailController.text;
                        final mobileNumber = mobileNumberController.text;
                        final password = passwordController.text;

                        firstNameController.text.isNotEmpty &&
                                lastNameController.text.isNotEmpty &&
                                emailController.text.isNotEmpty &&
                                mobileNumberController.text.isNotEmpty &&
                                passwordController.text.isNotEmpty
                            ? doRegister(
                                firstNameController.text,
                                lastNameController.text,
                                emailController.text,
                                mobileNumberController.text,
                                passwordController.text)
                            : Fluttertoast.showToast(
                                msg: 'All fields are required',
                                textColor: Colors.red);

                        // Add your logic for handling registration here
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.black54),
                      ),
                      child: Text(
                        "Register",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(height: 25),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Login()),
                      );
                    },
                    child: Text(
                      "Already have an account? Sign in",
                      style: TextStyle(
                        color: Colors.black54,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  doRegister(String firstName, String lastName, String email,
      String mobileNumber, String password) async {
    var res =
        await userRegister(firstName, lastName, email, mobileNumber, password);
    if (res.containsKey('success') && res['success']) {
      // Navigate to the Dashboard screen
      Fluttertoast.showToast(
          msg: 'Proceed on logging in.', textColor: Colors.red);
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => Login()));
    } else {
      Fluttertoast.showToast(msg: 'Please try again.', textColor: Colors.red);
    }
  }
}
