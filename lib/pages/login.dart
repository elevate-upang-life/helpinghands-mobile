import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:helping_hands/pages/register.dart';
import 'package:helping_hands/rest/rest-api.dart';
import 'package:helping_hands/widgets/navbar_routes.dart';

class Login extends StatefulWidget {
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool hidePassword = true;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return _loginUI(context);
  }

  Widget _loginUI(BuildContext context) {

    const customGreenColor = Color(0xFFbbe7d7);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFbbe7d7),
        body: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('lib/images/login.png'),
                    SizedBox(height: 15),
                    Text(

                      'Login to Helping Hands ',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,),// Set the background color to white

                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          hintText: "Email",
                          hintStyle: TextStyle(color: Colors.black54),
                          suffixIcon: Icon(
                            Icons.email,
                            color: Colors.black54,
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Email can't be empty.";
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,),// Set
                      child: TextFormField(
                        controller: passwordController,
                        obscureText: hidePassword,
                        decoration: InputDecoration(

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.black54),
                          ),
                          hintText: "Password",
                          hintStyle: TextStyle(color: Colors.black54),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                hidePassword = !hidePassword;
                              });
                            },
                            color: Colors.black54,
                            icon: Icon(
                              hidePassword
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Password can't be empty.";
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                    Center(
                      child: ElevatedButton(
                        onPressed: () {
                          final email = emailController.text;
                          final password = passwordController.text;
                          emailController.text.isNotEmpty &&
                                  passwordController.text.isNotEmpty
                              ? doLogin(emailController.text,
                                  passwordController.text)
                              : Fluttertoast.showToast(
                                  msg: 'All fields are required',
                                  textColor: Colors.red,
                                );
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.black54),
                        ),
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Register()),
                        );
                      },
                      child: Text(
                        "Don't have an account? Click me to sign up!",
                        style: TextStyle(
                          color: Colors.black54,

                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void doLogin(String email, String password) async {
    var res = await userLogin(email.trim(), password.trim());

    if (res.containsKey('success') && res['success']) {
      // Navigate to the Dashboard screen
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => NavBar()),
      );
    } else {
      Fluttertoast.showToast(
          msg: 'Email and password are not valid.', textColor: Colors.red);
    }
  }
}
