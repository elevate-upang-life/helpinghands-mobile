import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart'; // Import the image_picker package
import '../models/event.dart';
import '../rest/crud.dart';

class Edit extends StatefulWidget {
  final Event data;
  Edit({required this.data});

  @override
  State<Edit> createState() => _EditState();
}

class _EditState extends State<Edit> {
  var eventController = TextEditingController();
  var timeController = TextEditingController();
  var locationController = TextEditingController();
  var descriptionController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  File? selectedImage; // Stores the selected image file

  // Function to pick an image from the device's gallery
  Future<void> _pickImage() async {
    final imagePicker = ImagePicker();
    final pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      setState(() {
        selectedImage = File(pickedImage.path);
      });

      // Extract the original filename and extension
      final originalFileName = pickedImage.path.split('/').last;
      final originalFileExtension = originalFileName.split('.').last;

      // Use the extracted filename and extension when setting the filename in your request
      final filename = '$originalFileName.$originalFileExtension';

      // Now, you can use this `filename` when adding the image to your request
    }
  }




  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFFbbe7d7);
    return Scaffold(
      appBar: AppBar(
        title: Text(
            'Post Opportunities Here!'
        ),
        backgroundColor: customGreenColor,
        foregroundColor: Colors.black54,
      ),
      body: Center(
        child: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey, // Add the Form widget and the form key
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // Event Name
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: customGreenColor),
                                color: Colors.grey[200],
                              ),
                              margin: const EdgeInsets.symmetric(vertical: 8.0),
                              padding: const EdgeInsets.all(10.0),
                              child: TextFormField(
                                controller: eventController,
                                decoration: InputDecoration(
                                  labelText: "Event Name",
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Event Name is required';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            // Description
                            Container(
                              height: 120, // Increase the height to allow for multiline input
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: customGreenColor),
                                color: Colors.grey[200],
                              ),
                              margin: const EdgeInsets.symmetric(vertical: 8.0),
                              padding: const EdgeInsets.all(10.0),
                              child: TextFormField(
                                controller: descriptionController,
                                decoration: InputDecoration(
                                  labelText: "Description",
                                  border: InputBorder.none,
                                ),
                                maxLines: null, // Allows multiline input
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Description is required';
                                  }
                                  if (value.length > 60000) {
                                    return 'Description must be no more than 60,000 characters';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            // Time
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: customGreenColor),
                                color: Colors.grey[200],
                              ),
                              margin: const EdgeInsets.symmetric(vertical: 8.0),
                              padding: const EdgeInsets.all(10.0),
                              child: TextFormField(
                                controller: timeController,
                                decoration: InputDecoration(
                                  labelText: "Time",
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Time is required';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            // Location
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: customGreenColor),
                                color: Colors.grey[200],
                              ),
                              margin: const EdgeInsets.symmetric(vertical: 8.0),
                              padding: const EdgeInsets.all(10.0),
                              child: TextFormField(
                                controller: locationController,
                                decoration: InputDecoration(
                                  labelText: "Location",
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Location is required';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Column(
                              children: [
                                ElevatedButton.icon(
                                  onPressed: _pickImage,
                                  icon: Icon(Icons.upload),
                                  label: Text('Upload Image'),
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.grey[200],
                                    onPrimary: Colors.black54,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                ),
                                if (selectedImage != null)
                                  Column(
                                      children: [
                                        SizedBox(height: 10), // Add spacing
                                        Image.file(selectedImage!, height: MediaQuery.of(context).size.height * 0.2),
                                      ]
                                  ),
                                ElevatedButton.icon(
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        var data = {
                                          "eevent": eventController.text,
                                          "etime": timeController.text,
                                          "elocation": locationController.text,
                                          "edescription": descriptionController.text,
                                        };
                                        Crud.addEvent(data, selectedImage!);
                                      }
                                    },
                                    icon: Icon(Icons.post_add),
                                    label: Text('Post Opportunity'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.grey[200],
                                      onPrimary: Colors.black54,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    )
                                )
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
