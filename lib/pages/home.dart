import 'dart:ffi';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:helping_hands/models/event.dart';
import 'package:helping_hands/pages/history.dart';
import 'package:helping_hands/pages/opportunities.dart';
import 'package:helping_hands/rest/crud.dart';
import 'package:image_picker/image_picker.dart';
import 'package:helping_hands/pages/create.dart';
import 'package:helping_hands/pages/delete.dart';
import 'package:helping_hands/pages/update.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final eventController = TextEditingController();
  final timeController = TextEditingController();
  final locationController = TextEditingController();
  final descriptionController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  File? selectedImage;

  Future<void> _pickImage() async {
    final imagePicker = ImagePicker();
    final pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      setState(() {
        selectedImage = File(pickedImage.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFFbbe7d7);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey, // Add the Form widget and the form key
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: customGreenColor,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'VOLUNTEER.',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'with Purpose | with Heart',
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => Create()));
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width * .44,
                                height: 100,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.grey[200],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Center(
                                        child: Image.asset(
                                          'lib/images/create.png',
                                          width: 35,
                                          height: 35,
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Post',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                              ),
                                            ),
                                            SizedBox(height: 10,),
                                            Text(
                                              'Make the opportunities be seen by people',
                                              style: TextStyle(
                                                fontSize: 14, // You can adjust the font size
                                                color: Colors.grey, // You can adjust the color
                                              ),
                                              maxLines: 3, // Limit the number of lines to prevent overflow
                                              overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )

                                ),
                              ),
                            ),
                            SizedBox(width: 10,),
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => Attendance()));
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width * .44,
                                height: 100,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.grey[200],
                                ),
                                child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        Center(
                                          child: Image.asset(
                                            'lib/images/attendance.png',
                                            width: 35,
                                            height: 35,
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Attendance',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Text(
                                                'Monitor the logs of the attendees on an event',
                                                style: TextStyle(
                                                  fontSize: 14, // You can adjust the font size
                                                  color: Colors.grey, // You can adjust the color
                                                ),
                                                maxLines: 3, // Limit the number of lines to prevent overflow
                                                overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )

                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                     SizedBox(height: 10,),
                     GestureDetector(
                       onTap: (){
                         Navigator.push(context,
                             MaterialPageRoute(builder: (context) => Update()));
                       },
                       child: Container(
                         padding: EdgeInsets.all(8),
                         width: MediaQuery.of(context).size.width * 1,
                         height: 100,
                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(15),
                           color: Colors.grey[200],
                         ),
                         child: Row(
                           children: [
                             Image.asset('lib/images/post.png'),
                             SizedBox(width: 25,),
                             Expanded(
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Text(
                                     'My Postings',
                                     style: TextStyle(
                                       fontWeight: FontWeight.bold,
                                       fontSize: 20,
                                     ),
                                   ),
                                   SizedBox(height: 10),
                                   Text(
                                     'Allows users to easily edit and delete their postings, giving them full control over the contents.',
                                     style: TextStyle(
                                       fontSize: 14, // You can adjust the font size
                                       color: Colors.grey, // You can adjust the color
                                     ),
                                     maxLines: 4, // Limit the number of lines to prevent overflow
                                     overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                   ),
                                 ],
                               ),
                             )

                           ],
                         ),
                       ),
                     ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Available Opportunities',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold
                          ),),
                          TextButton(onPressed: (){
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => Opportunities()));
                          },
                              child: Text('See more',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold
                                ),)),
                        ],
                      ),
                      SizedBox(height: 10,),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.4,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                ),
                                child: Image.asset(
                                  'lib/images/sitiopatalan.jpg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),

                            SizedBox(width: 10),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.4,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                ),
                                child: Image.asset(
                                  'lib/images/covid.jpg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),

                            SizedBox(width: 10),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.4,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                ),
                                child: Image.asset(
                                  'lib/images/egay.jpg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10,),
                      Text('About Helping Hands',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold
                        ),),
                      SizedBox(height: 10,),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            width: MediaQuery.of(context).size.width * 1,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.grey[200],
                            ),
                            child: Row(
                              children: [
                                Image.asset('lib/images/hands.png'),
                                SizedBox(width: 25,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Compassionate Relief',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        'Helping Hands by PHINMA UPANG CSDL is a compassionate initiative in the Philippines that offers critical aid during typhoons, emergencies, and crises.',
                                        style: TextStyle(
                                          fontSize: 14, // You can adjust the font size
                                          color: Colors.grey, // You can adjust the color
                                        ),
                                        maxLines: 10, // Limit the number of lines to prevent overflow
                                        overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            padding: EdgeInsets.all(8),
                            width: MediaQuery.of(context).size.width * 1,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.grey[200],
                            ),
                            child: Row(
                              children: [
                                Image.asset('lib/images/help.png'),
                                SizedBox(width: 25,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Volunteer-Driven',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        'Powered by the University of Pangasinan (UPANG) Community, this volunteer-driven effort provides emergency relief supplies, shelter, food, clean water, medical assistance, and emotional support to disaster-affected areas.',
                                        style: TextStyle(
                                          fontSize: 14, // You can adjust the font size
                                          color: Colors.grey, // You can adjust the color
                                        ),
                                        maxLines: 10, // Limit the number of lines to prevent overflow
                                        overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            padding: EdgeInsets.all(8),
                            width: MediaQuery.of(context).size.width * 1,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.grey[200],
                            ),
                            child: Row(
                              children: [
                                Image.asset('lib/images/struggle.png'),
                                SizedBox(width: 25,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Resilience Building',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        'The organization also prioritizes disaster preparedness and resilience through educational workshops and drills.',
                                        style: TextStyle(
                                          fontSize: 14, // You can adjust the font size
                                          color: Colors.grey, // You can adjust the color
                                        ),
                                        maxLines: 10, // Limit the number of lines to prevent overflow
                                        overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            padding: EdgeInsets.all(8),
                            width: MediaQuery.of(context).size.width * 1,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.grey[200],
                            ),
                            child: Row(
                              children: [
                                Image.asset('lib/images/action.png'),
                                SizedBox(width: 25,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Collective Action',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        'With a dedicated team of volunteers, students, faculty, and professionals, Helping Hands stands as a testament to the strength of collective action and the enduring spirit of human compassion and resilience during adversity.',
                                        style: TextStyle(
                                          fontSize: 14, // You can adjust the font size
                                          color: Colors.grey, // You can adjust the color
                                        ),
                                        maxLines: 10, // Limit the number of lines to prevent overflow
                                        overflow: TextOverflow.ellipsis, // Add an ellipsis (...) for long text
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),





                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
