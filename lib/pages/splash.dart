

import 'package:flutter/material.dart';
import 'package:helping_hands/pages/login.dart';


class Splash extends StatefulWidget {

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
@override
void initState(){
  super.initState();
  _navigatetohome();
}

_navigatetohome() async {
  await Future.delayed(Duration(milliseconds: 2000), (){} );
  Navigator.pushReplacement(context as BuildContext, MaterialPageRoute(builder: (context) => Login()));
}
  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFF93d8bf);
    return Scaffold(
      backgroundColor: customGreenColor,
      body: Center(
        child: Image.asset('lib/images/splash.png'),
      ),
    );
  }
}
