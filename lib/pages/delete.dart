import 'package:flutter/material.dart';
import '../models/event.dart';
import '../rest/crud.dart';

class Delete extends StatefulWidget {
  const Delete({super.key});

  @override
  State<Delete> createState() => _DeleteState();
}

class _DeleteState extends State<Delete> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: Crud.getEvent(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              List<Event> edata = snapshot.data;

              return ListView.builder(
                  itemCount: edata.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: Icon(Icons.event_available),
                      title: Text("${edata[index].event}"),
                      subtitle: Text("${edata[index].time}"),
                      trailing: IconButton(
                          onPressed: ()  async{
                            await Crud.deleteEvent(edata[index].id);
                            edata.removeAt(index);
                            setState(() {});
                          },
                          icon: Icon(Icons.delete)),
                    );
                  });
            }
          }),
    );
  }
}
