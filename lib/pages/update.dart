import 'package:flutter/material.dart';
import 'package:helping_hands/models/event.dart';
import 'package:helping_hands/pages/edit.dart';
import '../rest/crud.dart';

class Update extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const customGreenColor = Color(0xFFbbe7d7);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          foregroundColor: Colors.black54,
          title: Text('Update & Delete',),
          backgroundColor: customGreenColor,
          bottom: TabBar(
            labelColor: Colors.black54,
            tabs: [
              Tab(text: 'Update'),
              Tab(text: 'Delete'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            UpdatePage(),
            DeletePage(),
          ],
        ),
      ),
    );
  }
}

class UpdatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Crud.getEvent(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          List<Event> edata = snapshot.data;

          return ListView.builder(
            itemCount: edata.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Icon(Icons.event_available),
                title: Text("${edata[index].event}"),
                subtitle: Text("${edata[index].time}"),
                trailing: IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Edit(data: edata[index]),
                      ),
                    );
                  },
                  icon: Icon(Icons.edit),
                ),
              );
            },
          );
        }
      },
    );
  }
}

class DeletePage extends StatefulWidget {
  @override
  _DeletePageState createState() => _DeletePageState();
}

class _DeletePageState extends State<DeletePage> {
  List<Event> edata = [];

  @override
  void initState() {
    super.initState();
    refreshData();
  }

  Future<void> refreshData() async {
    final events = await Crud.getEvent();
    setState(() {
      edata = events;
    });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refreshData,
      child: ListView.builder(
        itemCount: edata.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: Icon(Icons.event_available),
            title: Text("${edata[index].event}"),
            subtitle: Text("${edata[index].time}"),
            trailing: IconButton(
              onPressed: () async {
                await Crud.deleteEvent(edata[index].id);
                refreshData();
              },
              icon: Icon(Icons.delete),
            ),
          );
        },
      ),
    );
  }
}
