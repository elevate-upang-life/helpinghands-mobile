class Event {
  final String? id;
  final String? event;
  final String? time;
  final String? location;
  final String? image;
  final String? description;

  Event({this.event, this.time, this.location, this.id, this.image, this.description});
}
